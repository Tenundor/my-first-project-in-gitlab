# My first project in GitLab

Exploring platform capabilities

```mermaid
classDiagram
	LTComponent "0..*"--"1" ComponentType
	LTComponent "0..*"--"1" ComponentMaterial
	
	class LTComponent{
		+String label 
		+String name  
		+String description  
		+ComponentType[1] type 
		+ComponentMaterial[1] material
		+__str__(self) String
	}
	
	class ComponentType{
		+ String name
		+__str__(self) String
	}
	
	class ComponentMaterial{
		+String name
		+__str__(self) String
	}
end
```
